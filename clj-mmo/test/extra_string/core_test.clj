; SPDX-FileCopyrightText: 2021 AdrianneJupi
; SPDX-FileCopyrightText: 2021 Del_Messelyn
; SPDX-FileCopyrightText: 2021 leirda
;
; SPDX-License-Identifier: GPL-3.0-or-later

(ns extra-string.core-test
  (:require
    [clojure.test :refer :all]
    [clojure.test.check :as tc]
    [clojure.test.check.generators :as gen]
    [clojure.test.check.properties :as prop]
    [clojure.test.check.clojure-test :refer [defspec]]
    [extra-string.core :refer :all]))

(deftest intersperse-test 
  (testing "The intersperse function"
    (testing "should return an empty string"
      (is (= "" (intersperse "" '()))))

    (testing "should insert spaces between letters"
      (is (= "a b c d" (intersperse " " '("a" "b" "c" "d")))))

    (testing "should insert spaces between words"
      (is (= "ao midori shiro aka kuro" (intersperse " " '("ao" "midori" "shiro" "aka" "kuro")))))

    (testing "should insert one dash between each word"
      (is (= "ichi-ni-san-yon-go" (intersperse "-" '("ichi" "ni" "san" "yon" "go")))))

    (testing "should insert two dashes between each word"
      (is (= "ichi--ni--san--yon--go" (intersperse "--" '("ichi" "ni" "san" "yon" "go")))))

    (testing "should work even with empty word"
      (is (= "umi sora daichi" (intersperse " "  '("umi" "sora" "" "daichi")))))))

; Définir une fonction qui prend en paramètre deux listes et qui vérifie qu’il
; y a au moins un élément en commun entre ces deux listes.
(deftest some-in-common?-test
  (testing "The some-in-common? function"
    (testing "should return true when two colls are the same"
      (is (some-in-common? '("pompier") '("pompier"))))
    
    (testing "should return false when two colls are different"
      (is (not (some-in-common? '("pompier") '("peintre")))))
    
    (testing "should return true when the single element in a coll is contained within the other coll"
      (is (some-in-common? '("peintre") '("pompier" "peintre")))
      (is (some-in-common? '("peintre") '("peintre" "pompier")))
      (is (some-in-common? '("peintre" "pompier") '("pompier"))))
    
    (testing "should return false when a coll have twice the same element"
      (is (not (some-in-common? '("peintre" "peintre") '("pompier")))))))


; Fuzzy tests
(defspec pos-and-neg-int-not-in-common 100
  (prop/for-all [coll1 (gen/list gen/s-neg-int)
                 coll2 (gen/list gen/s-pos-int)]
    (not (some-in-common? coll1 coll2))))

(defspec lower-and-upper-strings-not-in-common 100
  (letfn [(string-alpha []
            (gen/fmap clojure.string/join (gen/vector gen/char-alpha)))]
  (prop/for-all [coll1 (gen/list (gen/not-empty (gen/fmap #(clojure.string/lower-case %) (string-alpha))))
                 coll2 (gen/list (gen/not-empty (gen/fmap #(clojure.string/upper-case %) (string-alpha))))]
    (not (some-in-common? coll1 coll2)))))

(run-tests)
