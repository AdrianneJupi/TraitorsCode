; SPDX-FileCopyrightText: 2021 AdrianneJupi
; SPDX-FileCopyrightText: 2021 Del_Messelyn
; SPDX-FileCopyrightText: 2021 leirda
;
; SPDX-License-Identifier: GPL-3.0-or-later

(defproject clj-mmo "0.1.0-SNAPSHOT"
  :description "A very serious MEUPORGUE with micro-transactions in it."
  :url "https://codeberg.org/AdrianneJupi/TraitorsCode"
  :license {:name "GPL-3.0-or-later"
            :url "https://www.gnu.org/licenses/gpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/test.check "1.1.0"]]
  :dev-dependencies [[vimclojure/server "latest"]]
  :main ^:skip-aot clj-mmo.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
