; SPDX-FileCopyrightText: 2021 AdrianneJupi
; SPDX-FileCopyrightText: 2021 Del_Messelyn
; SPDX-FileCopyrightText: 2021 leirda
;
; SPDX-License-Identifier: GPL-3.0-or-later

(ns clj-mmo.core
  (:gen-class))


(defn insert-spaces [names]
  (loop [n names acc "Hello"]
    (if (not (empty? n))
      (recur (rest n) (str acc " " (first n)))
      acc)))

(defn grite
  ([] "Hello world")
  ([& names] (insert-spaces names)))


(defn -main [& opts] 
  (println (grite "adriel" "del" "luc")))


