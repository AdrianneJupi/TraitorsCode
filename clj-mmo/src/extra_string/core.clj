; SPDX-FileCopyrightText: 2021 AdrianneJupi
; SPDX-FileCopyrightText: 2021 Del_Messelyn
; SPDX-FileCopyrightText: 2021 leirda
;
; SPDX-License-Identifier: GPL-3.0-or-later

(ns extra-string.core)

(defn intersperse [sep words]
  (loop [acc "" [word & words] (remove #(empty? %) words)]
    (if (string? word)
      (if (empty? acc)
        (recur word words)
        (recur (str acc sep word) words))
      acc)))

(defn some-in-common? [coll1 coll2]
  (some
    #(> % 1)
    (let [ucoll1 (distinct coll1)
          ucoll2 (distinct coll2)]
    (->> (concat ucoll1 ucoll2)
      (frequencies) (vals)))))
