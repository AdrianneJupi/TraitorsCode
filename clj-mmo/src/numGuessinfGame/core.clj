; SPDX-FileCopyrightText: 2021 AdrianneJupi
; SPDX-FileCopyrightText: 2021 Del_Messelyn
; SPDX-FileCopyrightText: 2021 leirda
;
; SPDX-License-Identifier: GPL-3.0-or-later

(ns numGuessinfGame.core)

(defn input-valid? [input]
  (or (integer? input)
      (and (float? input)
           (= 0.0 (mod (* 10 input) 10)))))           

(defn prompt [question]
  (do (println (str question " (Y/n)"))
      (let [inpuf (read-line)]
        (some #(= inpuf %) '("y" "Y" "")))))

(defn game [rangf]
  (let [number (rand-int (inc rangf))]
    (loop [attempts 0 message "Guess a number:"]
      (println message)
      (let [guess (read-string (read-line))]
        (if (input-valid? guess)
          (cond (= guess number)
                (do (println (str "Congratulations! You won in " (inc attempts) " tries!"))
                    (if (prompt "Play again?")
                      (game rangf)
                      "Good bye!"))
                (> guess number)
                (recur (inc attempts) "Too high! Please try again:")
                (< guess number)
                (recur (inc attempts) "Too low! Please try again:"))
          (recur attempts "Invalid! Please try again:"))))))

