; SPDX-FileCopyrightText: 2021 AdrianneJupi
; SPDX-FileCopyrightText: 2021 Del_Messelyn
; SPDX-FileCopyrightText: 2021 leirda
;
; SPDX-License-Identifier: GPL-3.0-or-later

(ns numGuessinfGame.core)

;; Helpers functions

(defn display [message]
  (do (print message) (flush)))

(defn prompt-int [message]
  (do (display message)
      (let [inpuf (read-line)]
        (if (every? #(Character/isDigit %)
                    (map char inpuf))
          (Integer/parseInt inpuf)
          (recur "Invalid! Please submit a positive integer: ")))))

(defn prompt [question]
  (do (display question)
      (display " (Y/n) ")
      (let [inpuf (read-line)]
        ((complement not-any?) #{inpuf} '("y" "Y" "")))))

;; Game logic

; What should find-number return?
(defn find-number
  ([answer] (find-number 0 answer "Guess!: ")) 
  ([attempts answer message]
   (let [Uguess (prompt-int message)]
     (cond (= Uguess answer)
           (inc attempts)
           (> Uguess answer)
           (recur (inc attempts) answer "Too high! Try again!: ")
           (< Uguess answer)
           (recur (inc attempts) answer "Too low! Try again!: "))))) 

(defn game
  ([] (game (prompt-int "Please choose max value: ")))
  ([rangf] (do (display (str "Guess a number between 0 and " rangf "."))
      (newline)
      (display (str "Congratulations! You won in "
                    (find-number (rand-int (inc rangf)))
                    " tries!"))
      (newline)
      (if (prompt "Play again?")

        (recur (prompt-int "Please choose new max value: "))
        (do (display "Good bye!")
            (newline))))))

