<!--
SPDX-FileCopyrightText: 2021 AdrianneJupi
SPDX-FileCopyrightText: 2021 Del_Messelyn
SPDX-FileCopyrightText: 2021 leirda

SPDX-License-Identifier: GPL-3.0-or-later
-->

# clj-mmo

Our free (as in free beer) MMO but with microtransactions in it.
made with ♥

